﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BankGUI.Controllers
{
    public class NewTransferController : Controller
    {

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(TransferReference.NewTransfer collection)
        {
            var client = new WebClient();
            var dataString = JsonConvert.SerializeObject(collection);

            client.Headers.Add(HttpRequestHeader.ContentType, "application/json");

            var content = client.UploadString("http://localhost:60134/TransferService.svc/NewTransfer/", dataString);

            return View();
        }
    }
}
