﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BankRESTAPI
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="T1808M_PhamNgocAnh")]
	public partial class BankDataDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertTransferHistory(TransferHistory instance);
    partial void UpdateTransferHistory(TransferHistory instance);
    partial void DeleteTransferHistory(TransferHistory instance);
    partial void InsertClient(Client instance);
    partial void UpdateClient(Client instance);
    partial void DeleteClient(Client instance);
    partial void InsertPartner(Partner instance);
    partial void UpdatePartner(Partner instance);
    partial void DeletePartner(Partner instance);
    #endregion
		
		public BankDataDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["T1808M_PhamNgocAnhConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public BankDataDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public BankDataDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public BankDataDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public BankDataDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<TransferHistory> TransferHistories
		{
			get
			{
				return this.GetTable<TransferHistory>();
			}
		}
		
		public System.Data.Linq.Table<Client> Clients
		{
			get
			{
				return this.GetTable<Client>();
			}
		}
		
		public System.Data.Linq.Table<Partner> Partners
		{
			get
			{
				return this.GetTable<Partner>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TransferHistory")]
	public partial class TransferHistory : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _transferId;
		
		private string _transferName;
		
		private bool _transferType;
		
		private int _billCode;
		
		private int _cliebtId;
		
		private int _partnerId;
		
		private System.Nullable<System.DateTime> _created_date;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OntransferIdChanging(int value);
    partial void OntransferIdChanged();
    partial void OntransferNameChanging(string value);
    partial void OntransferNameChanged();
    partial void OntransferTypeChanging(bool value);
    partial void OntransferTypeChanged();
    partial void OnbillCodeChanging(int value);
    partial void OnbillCodeChanged();
    partial void OncliebtIdChanging(int value);
    partial void OncliebtIdChanged();
    partial void OnpartnerIdChanging(int value);
    partial void OnpartnerIdChanged();
    partial void Oncreated_dateChanging(System.Nullable<System.DateTime> value);
    partial void Oncreated_dateChanged();
    #endregion
		
		public TransferHistory()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_transferId", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int transferId
		{
			get
			{
				return this._transferId;
			}
			set
			{
				if ((this._transferId != value))
				{
					this.OntransferIdChanging(value);
					this.SendPropertyChanging();
					this._transferId = value;
					this.SendPropertyChanged("transferId");
					this.OntransferIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_transferName", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string transferName
		{
			get
			{
				return this._transferName;
			}
			set
			{
				if ((this._transferName != value))
				{
					this.OntransferNameChanging(value);
					this.SendPropertyChanging();
					this._transferName = value;
					this.SendPropertyChanged("transferName");
					this.OntransferNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_transferType", DbType="Bit NOT NULL")]
		public bool transferType
		{
			get
			{
				return this._transferType;
			}
			set
			{
				if ((this._transferType != value))
				{
					this.OntransferTypeChanging(value);
					this.SendPropertyChanging();
					this._transferType = value;
					this.SendPropertyChanged("transferType");
					this.OntransferTypeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_billCode", DbType="Int NOT NULL")]
		public int billCode
		{
			get
			{
				return this._billCode;
			}
			set
			{
				if ((this._billCode != value))
				{
					this.OnbillCodeChanging(value);
					this.SendPropertyChanging();
					this._billCode = value;
					this.SendPropertyChanged("billCode");
					this.OnbillCodeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_cliebtId", DbType="Int NOT NULL")]
		public int cliebtId
		{
			get
			{
				return this._cliebtId;
			}
			set
			{
				if ((this._cliebtId != value))
				{
					this.OncliebtIdChanging(value);
					this.SendPropertyChanging();
					this._cliebtId = value;
					this.SendPropertyChanged("cliebtId");
					this.OncliebtIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_partnerId", DbType="Int NOT NULL")]
		public int partnerId
		{
			get
			{
				return this._partnerId;
			}
			set
			{
				if ((this._partnerId != value))
				{
					this.OnpartnerIdChanging(value);
					this.SendPropertyChanging();
					this._partnerId = value;
					this.SendPropertyChanged("partnerId");
					this.OnpartnerIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_created_date", DbType="DateTime")]
		public System.Nullable<System.DateTime> created_date
		{
			get
			{
				return this._created_date;
			}
			set
			{
				if ((this._created_date != value))
				{
					this.Oncreated_dateChanging(value);
					this.SendPropertyChanging();
					this._created_date = value;
					this.SendPropertyChanged("created_date");
					this.Oncreated_dateChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Client")]
	public partial class Client : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _clientId;
		
		private int _clientCode;
		
		private string _clientPassword;
		
		private int _pin;
		
		private decimal _deposits;
		
		private System.Nullable<System.DateTime> _created_date;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnclientIdChanging(int value);
    partial void OnclientIdChanged();
    partial void OnclientCodeChanging(int value);
    partial void OnclientCodeChanged();
    partial void OnclientPasswordChanging(string value);
    partial void OnclientPasswordChanged();
    partial void OnpinChanging(int value);
    partial void OnpinChanged();
    partial void OndepositsChanging(decimal value);
    partial void OndepositsChanged();
    partial void Oncreated_dateChanging(System.Nullable<System.DateTime> value);
    partial void Oncreated_dateChanged();
    #endregion
		
		public Client()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_clientId", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int clientId
		{
			get
			{
				return this._clientId;
			}
			set
			{
				if ((this._clientId != value))
				{
					this.OnclientIdChanging(value);
					this.SendPropertyChanging();
					this._clientId = value;
					this.SendPropertyChanged("clientId");
					this.OnclientIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_clientCode", DbType="Int NOT NULL")]
		public int clientCode
		{
			get
			{
				return this._clientCode;
			}
			set
			{
				if ((this._clientCode != value))
				{
					this.OnclientCodeChanging(value);
					this.SendPropertyChanging();
					this._clientCode = value;
					this.SendPropertyChanged("clientCode");
					this.OnclientCodeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_clientPassword", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string clientPassword
		{
			get
			{
				return this._clientPassword;
			}
			set
			{
				if ((this._clientPassword != value))
				{
					this.OnclientPasswordChanging(value);
					this.SendPropertyChanging();
					this._clientPassword = value;
					this.SendPropertyChanged("clientPassword");
					this.OnclientPasswordChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_pin", DbType="Int NOT NULL")]
		public int pin
		{
			get
			{
				return this._pin;
			}
			set
			{
				if ((this._pin != value))
				{
					this.OnpinChanging(value);
					this.SendPropertyChanging();
					this._pin = value;
					this.SendPropertyChanged("pin");
					this.OnpinChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_deposits", DbType="Decimal(18,0) NOT NULL")]
		public decimal deposits
		{
			get
			{
				return this._deposits;
			}
			set
			{
				if ((this._deposits != value))
				{
					this.OndepositsChanging(value);
					this.SendPropertyChanging();
					this._deposits = value;
					this.SendPropertyChanged("deposits");
					this.OndepositsChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_created_date", DbType="DateTime")]
		public System.Nullable<System.DateTime> created_date
		{
			get
			{
				return this._created_date;
			}
			set
			{
				if ((this._created_date != value))
				{
					this.Oncreated_dateChanging(value);
					this.SendPropertyChanging();
					this._created_date = value;
					this.SendPropertyChanged("created_date");
					this.Oncreated_dateChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Partner")]
	public partial class Partner : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _partnerId;
		
		private int _partnerCode;
		
		private string _partnerPassword;
		
		private decimal _deposits;
		
		private System.Nullable<System.DateTime> _created_date;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnpartnerIdChanging(int value);
    partial void OnpartnerIdChanged();
    partial void OnpartnerCodeChanging(int value);
    partial void OnpartnerCodeChanged();
    partial void OnpartnerPasswordChanging(string value);
    partial void OnpartnerPasswordChanged();
    partial void OndepositsChanging(decimal value);
    partial void OndepositsChanged();
    partial void Oncreated_dateChanging(System.Nullable<System.DateTime> value);
    partial void Oncreated_dateChanged();
    #endregion
		
		public Partner()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_partnerId", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int partnerId
		{
			get
			{
				return this._partnerId;
			}
			set
			{
				if ((this._partnerId != value))
				{
					this.OnpartnerIdChanging(value);
					this.SendPropertyChanging();
					this._partnerId = value;
					this.SendPropertyChanged("partnerId");
					this.OnpartnerIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_partnerCode", DbType="Int NOT NULL")]
		public int partnerCode
		{
			get
			{
				return this._partnerCode;
			}
			set
			{
				if ((this._partnerCode != value))
				{
					this.OnpartnerCodeChanging(value);
					this.SendPropertyChanging();
					this._partnerCode = value;
					this.SendPropertyChanged("partnerCode");
					this.OnpartnerCodeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_partnerPassword", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string partnerPassword
		{
			get
			{
				return this._partnerPassword;
			}
			set
			{
				if ((this._partnerPassword != value))
				{
					this.OnpartnerPasswordChanging(value);
					this.SendPropertyChanging();
					this._partnerPassword = value;
					this.SendPropertyChanged("partnerPassword");
					this.OnpartnerPasswordChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_deposits", DbType="Decimal(18,0) NOT NULL")]
		public decimal deposits
		{
			get
			{
				return this._deposits;
			}
			set
			{
				if ((this._deposits != value))
				{
					this.OndepositsChanging(value);
					this.SendPropertyChanging();
					this._deposits = value;
					this.SendPropertyChanged("deposits");
					this.OndepositsChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_created_date", DbType="DateTime")]
		public System.Nullable<System.DateTime> created_date
		{
			get
			{
				return this._created_date;
			}
			set
			{
				if ((this._created_date != value))
				{
					this.Oncreated_dateChanging(value);
					this.SendPropertyChanging();
					this._created_date = value;
					this.SendPropertyChanged("created_date");
					this.Oncreated_dateChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
